import React from "react";
import { BeatLoader } from "react-spinners";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Alert,
  AlertDescription,
  AlertDialog,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogOverlay,
  AlertIcon,
  AlertTitle,
  Box,
  Button,
  Card,
  CardHeader,
  CardBody,
  Center,
  ChakraProvider,
  CircularProgress,
  Divider,
  Flex,
  FormControl,
  HStack,
  Heading,
  Image,
  Input,
  Link,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  PopoverArrow,
  Progress,
  SkeletonCircle,
  Spacer,
  Spinner,
  Stack,
  StackDivider,
  Stat,
  StatHelpText,
  StatLabel,
  StatNumber,
  Step,
  StepIcon,
  StepIndicator,
  StepSeparator,
  StepStatus,
  Stepper,
  Switch,
  Table,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tooltip,
  Tr,
  VStack,
  theme,
  useSteps,
  StatGroup,
} from "@chakra-ui/react";
import {
  ArrowRightIcon,
  ExternalLinkIcon,
  CheckIcon,
  CheckCircleIcon,
  InfoIcon,
  WarningIcon,
  LockIcon,
} from "@chakra-ui/icons";
import "@fontsource/roboto";
import Icon from "react-crypto-icons";
import axios from "axios";
import TxTable from "./TxTable";

////////////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////////////

// ProgressBlocks is the max block age in Thorchain blocks since the finalized height
// for which we show observation progress - older just displays the count.
const ProgressBlocks = 1800; // 3h

// URLs
let ThornodeURL = "https://thornode.ninerealms.com";
let MidgardURL = "https://midgard.ninerealms.com";
const ShapeshiftAPIURLs = {
  BTC: "https://api.bitcoin.shapeshift.com",
  BCH: "https://api.bitcoincash.shapeshift.com",
  LTC: "https://api.litecoin.shapeshift.com",
  DOGE: "https://api.dogecoin.shapeshift.com",
  ETH: "https://api.ethereum.shapeshift.com",
  AVAX: "https://api.avalanche.shapeshift.com",
};
const UTXOClientURLs = {
  BTC: "https://bitcoin.ninerealms.com",
  BCH: "https://bitcoin-cash.ninerealms.com",
  LTC: "https://litecoin.ninerealms.com",
  DOGE: "https://dogecoin.ninerealms.com",
};

const RuneAsset = parseAsset("THOR.RUNE");

////////////////////////////////////////////////////////////////////////////////////////
// Helpers
////////////////////////////////////////////////////////////////////////////////////////

function isValidTxID(txid) {
  return txid && txid.length === 64;
}

function millisecondsToDHMS(ms, short) {
  // Convert to seconds
  let seconds = Math.floor(ms / 1000);

  let days = Math.floor(seconds / 86400);
  seconds %= 86400;
  let hours = Math.floor(seconds / 3600);
  seconds %= 3600;
  let minutes = Math.floor(seconds / 60);
  seconds = Math.floor(seconds % 60);

  const components = [];

  if (days > 0) {
    components.push(`${days}d`);
  }
  if (hours > 0) {
    components.push(`${String(hours).padStart(2, "0")}h`);
  }
  if (minutes > 0) {
    components.push(`${String(minutes).padStart(2, "0")}m`);
  }
  if (!short || components.length === 0) {
    components.push(`${String(seconds).padStart(2, "0")}s`);
  }

  if (short) {
    for (let i = 0; i < components.length - 2; i++) {
      components.pop();
    }
  }

  return components.join(" ");
}

function runescanURL(path, network) {
  const url = `https://runescan.io/${path}`;
  if (network) {
    return `${url}?network=${network}`;
  }
  return url;
}

function hashCode(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    const char = str.charCodeAt(i);
    hash = (hash << 5) - hash + char;
  }
  return hash;
}

let vaultColors = {};

function colorizeVault(vault) {
  if (!vault) return null;

  if (vaultColors[vault]) {
    return `${vaultColors[vault]}.200`;
  }

  const allChakraColors = [
    "red",
    "orange",
    "green",
    "teal",
    "blue",
    "cyan",
    "purple",
  ];

  const hash = hashCode(vault);
  let index = Math.abs(hash) % allChakraColors.length;
  let selectedColor = allChakraColors[index];

  // iterate until we find an unused color
  const usedColors = Object.values(vaultColors);
  if (usedColors.length < allChakraColors.length) {
    while (usedColors.includes(selectedColor)) {
      index = (index + 1) % allChakraColors.length;
      selectedColor = allChakraColors[index];
    }
  }

  vaultColors[vault] = selectedColor;

  return `${vaultColors[vault]}.200`;
}

function stripTxID(txid) {
  // strip 0x prefix
  if (txid.startsWith("0x")) {
    txid = txid.slice(2);
  }
  return txid;
}

function haltTooltip(haltHeight, chainHeight) {
  if (haltHeight <= chainHeight) {
    return "";
  }

  const diff = haltHeight - chainHeight;
  const time = millisecondsToDHMS(diff * blockMilliseconds("THOR"), true);
  return `Scheduled halt at height ${haltHeight} (~${time})`;
}

////////////////////////////////////////////////////////////////////////////////////////
// Chain Specific Helpers
////////////////////////////////////////////////////////////////////////////////////////

function shortAddress(address, chain) {
  // assume thorname if short address
  if (!address || address.length < 34) {
    return address;
  }

  switch (chain) {
    case "ETH":
    case "BSC":
    case "AVAX":
    case "BCH":
    case "DOGE":
    case "BTC":
      return address.slice(0, 2) + "..." + address.slice(-4);
    case "LTC":
    case "BNB":
      return address.slice(0, 3) + "..." + address.slice(-4);
    case "THOR":
      return address.slice(0, 4) + "..." + address.slice(-4);
    case "GAIA":
      return address.slice(0, 6) + "..." + address.slice(-4);
    default:
      return "";
  }
}

function txExplorerLink(txid, asset, network) {
  switch (asset?.synth ? "THOR" : asset?.chain) {
    case "ETH":
      return `https://etherscan.io/tx/0x${txid}`;
    case "BSC":
      return `https://bscscan.com/tx/0x${txid}`;
    case "BNB":
      return `https://explorer.binance.org/tx/${txid}`;
    case "AVAX":
      return `https://cchain.explorer.avax.network/tx/0x${txid}`;
    case "LTC":
      return `https://live.blockcypher.com/ltc/tx/${txid.toLowerCase()}`;
    case "BTC":
      return `https://mempool.space/tx/${txid.toLowerCase()}`;
    case "DOGE":
      return `https://live.blockcypher.com/doge/tx/${txid.toLowerCase()}`;
    case "THOR":
      return runescanURL(`tx/${txid}`, network);
    case "GAIA":
      return `https://www.mintscan.io/cosmos/txs/${txid}`;
    case "BCH": // no blockcypher explorer for BCH
      return `https://blockchain.com/bch/tx/${txid}`;
    default:
      return "";
  }
}

function addressExplorerLink(address, asset, network) {
  switch (asset?.synth ? "THOR" : asset?.chain) {
    case "ETH":
      return `https://etherscan.io/address/${address}`;
    case "BSC":
      return `https://bscscan.com/address/${address}`;
    case "AVAX":
      return `https://cchain.explorer.avax.network/address/${address}`;
    case "LTC":
      return `https://live.blockcypher.com/ltc/address/${address}`;
    case "BTC":
      return `https://mempool.space/address/${address}`;
    case "BCH":
      return `https://live.blockcypher.com/bch/address/${address}`;
    case "DOGE":
      return `https://live.blockcypher.com/doge/address/${address}`;
    case "THOR":
      return runescanURL(`address/${address}`, network);
    case "GAIA":
      return `https://www.mintscan.io/cosmos/account/${address}`;
    default:
      return "";
  }
}

function requiresConfirmations(asset) {
  if (!asset || asset?.synth) {
    return false;
  }
  switch (asset.chain) {
    case "BTC":
    case "ETH":
    case "BCH":
    case "LTC":
    case "DOGE":
      return true;
    default:
      return false;
  }
}

// nativeTx returns a small amount of native tx data we are unable to retrieve from the
// default thornode and midgard endpoints.
async function nativeTx(txid, chain) {
  switch (chain) {
    case "THOR": {
      const res = await axios.get(
        `${ThornodeURL}/cosmos/tx/v1beta1/txs/${txid}`,
      );

      // the first transfer event is gas
      const gasEvent = atob(
        res.data.tx_response.events
          .find((e) => e.type === "transfer")
          .attributes.find((a) => atob(a.key) === "amount").value,
      );

      // ensure gas ends in "rune" and remove the "rune" suffix
      const gas = gasEvent.endsWith("rune") ? gasEvent.slice(0, -4) : null;

      return {
        gas: gas,
        gasAsset: RuneAsset,
      };
    }
    case "BTC":
    case "DOGE":
    case "BCH":
    case "LTC": {
      const res = await axios.get(
        `${ShapeshiftAPIURLs[chain]}/api/v1/tx/${txid}`,
      );

      let tx = {
        gas: parseInt(res.data.fee),
        gasAsset: parseAsset(`${chain}.${chain}`),
        confirmations: res.data.confirmations,
        amount: parseInt(res.data.value),
      };

      // get blockstats rpc
      if (res.data.blockHash && chain !== "DOGE") {
        const blockstats = await axios.post(`${UTXOClientURLs[chain]}`, {
          jsonrpc: "2.0",
          id: "1",
          method: "getblockstats",
          params: [res.data.blockHash, ["totalfee", "subsidy"]],
        });
        let feeAndSubsidy =
          blockstats.data.result.totalfee + blockstats.data.result.subsidy;

        // BCH is not in 1e8
        if (chain === "BCH") {
          feeAndSubsidy = feeAndSubsidy * 1e8;
        }

        // approximate - actual depends on total amount to asgards in block
        tx.confirmationsRequired = Math.floor(tx.amount / feeAndSubsidy);
      }

      return tx;
    }

    case "ETH":
    case "AVAX":
      const res = await axios.get(
        `${ShapeshiftAPIURLs[chain]}/api/v1/tx/${txid}`,
      );

      // TODO: handle tokens, this just works for ETH
      const confirmationsRequired = Math.floor(
        Math.max(2, (parseInt(res.data.value) * 2) / 3e18),
      );

      return {
        gas: parseInt(res.data.fee),
        gasAsset: parseAsset(`${chain}.${chain}`),
        confirmations: res.data.confirmations,
        confirmationsRequired: confirmationsRequired,
      };

    default:
      return null;
  }
}

function blockMilliseconds(chain) {
  switch (chain) {
    case "BTC":
      return 600_000;
    case "BCH":
      return 600_000;
    case "LTC":
      return 150_000;
    case "DOGE":
      return 60_000;
    case "ETH":
      return 12_000;
    case "THOR":
      return 6_000;
    case "GAIA":
      return 6_000;
    case "AVAX":
      return 3_000;
    case "BSC":
      return 3_000;
    case "BNB":
      return 500;
    default:
      return 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////////
// Memos
////////////////////////////////////////////////////////////////////////////////////////

function parseMemo(memo) {
  if (!memo) return {};

  // SWAP:ASSET:DESTADDR:LIM/INTERVAL/QUANTITY:AFFILIATE:FEE
  const parts = memo.split(":");
  const [limit, interval, quantity] = parts[3] ? parts[3].split("/") : [];

  return {
    type: parts[0] || null,
    asset: parts[1] || null,
    destAddr: parts[2] || null,
    limit: limit || null, // null if not present
    interval: parseInt(interval) || null, // null if not present
    quantity: parseInt(quantity) || null, // null if not present
    affiliate: parts[4] || null, // null if not present
    fee: parts[5] || null, // null if not present
  };
}

////////////////////////////////////////////////////////////////////////////////////////
// Assets
////////////////////////////////////////////////////////////////////////////////////////

function parseAsset(asset, pools) {
  if (!asset) return null;

  // asset with / is synth
  let sep = ".";
  let synth = false;
  if (asset.includes("/")) {
    synth = true;
    sep = "/";
  }

  // fuzzy match
  let chain = "";
  let symbol = "";
  if (asset.split(sep).length === 1) {
    switch (asset.split(sep)[0].toLowerCase()) {
      case "a":
        chain = "AVAX";
        symbol = "AVAX";
        break;
      case "b":
        chain = "BTC";
        symbol = "BTC";
        break;
      case "c":
        chain = "BCH";
        symbol = "BCH";
        break;
      case "n":
        chain = "BNB";
        symbol = "BNB";
        break;
      case "s":
        chain = "BSC";
        symbol = "BNB";
        break;
      case "d":
        chain = "DOGE";
        symbol = "DOGE";
        break;
      case "e":
        chain = "ETH";
        symbol = "ETH";
        break;
      case "g":
        chain = "GAIA";
        symbol = "ATOM";
        break;
      case "l":
        chain = "LTC";
        symbol = "LTC";
        break;
      case "r":
        chain = "THOR";
        symbol = "RUNE";
        break;
      default:
        chain = "";
        symbol = "";
    }
  } else {
    chain = asset.split(sep)[0].toUpperCase();
    symbol = asset.split(sep)[1].split("-")[0].toUpperCase();
  }

  let parsedAsset = { chain: chain, symbol: symbol, address: "", synth: synth };

  if (asset.includes("-")) {
    parsedAsset.address = asset.split(sep)[1].split("-")[1];

    // attempt to fuzzy match address
    if (pools && !(assetString(parsedAsset) in pools)) {
      Object.values(pools).forEach((pool) => {
        if (
          pool.asset.chain === chain &&
          pool.asset.symbol === symbol &&
          pool.asset.address.endsWith(parsedAsset.address)
        ) {
          parsedAsset.address = pool.asset.address;
        }
      });
    }
  }

  return parsedAsset;
}

function assetString(asset) {
  const sep = asset.synth ? "/" : ".";
  let assetString = `${asset.chain}${sep}${asset.symbol}`;
  if (asset.address) {
    assetString += `-${asset.address}`;
  }
  return assetString;
}

function assetChainSymbol(asset) {
  if (!asset) return "";
  const sep = asset.synth ? "/" : ".";
  return `${asset.chain}${sep}${asset.symbol}`;
}

////////////////////////////////////////////////////////////////////////////////////////
// Amounts
////////////////////////////////////////////////////////////////////////////////////////

// TODO: use value in network response after v1.121.
function usdPerRune(pools) {
  let asset = 0;
  let rune = 0;

  const anchorPools = [
    "ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48",
    "ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7",
    "AVAX.USDC-0XB97EF9EF8734C71904D8002F8B6BC66DD9C48A6E",
    "BNB.BUSD-BD1",
  ];
  anchorPools.forEach((pool) => {
    if (pools[pool]) {
      asset += parseInt(pools[pool].balance_asset);
      rune += parseInt(pools[pool].balance_rune);
    }
  });

  return asset / rune / 1e8;
}

function amountToUSD(amount, asset, pools) {
  if (!amount || !asset || !pools) return;

  const l1Asset = { ...asset, synth: false };

  let runeValue = amount;
  if (asset.chain !== "THOR" || asset.symbol !== "RUNE") {
    const pool = pools[assetString(l1Asset)];
    runeValue = pool ? (amount * pool.balance_rune) / pool.balance_asset : 0;
  }

  return runeValue * usdPerRune(pools);
}

function usdString(usd, round = true) {
  return usd
    ? usd.toLocaleString("en-US", {
        style: "currency",
        currency: "USD",
        maximumFractionDigits: round ? 0 : 2,
      })
    : null;
}

////////////////////////////////////////////////////////////////////////////////////////
// State
////////////////////////////////////////////////////////////////////////////////////////

// makeState takes all the endpoint responses and builds a state object in a structure
// which maps logically to the user interface.
//
// TODO: The status response should include the following to skip the details request:
// - inbound finalised height
// - outbound scheduled height
// - outbound observation counts
// - outbound finalised height
// - "actions" from the details should be in status
function makeState(
  status,
  pools,
  nativeIn,
  nativeOut,
  actions,
  inDetails,
  outDetails,
) {
  if (!status?.tx || !pools) {
    // if this is the outbound prompt to redirect
    if (inDetails?.tx.tx.memo.startsWith("OUT:")) {
      return {
        isOutbound: true,
        inbound: inDetails.tx.tx.memo.split(":")[1],
      };
    }
    return;
  }

  const memo = parseMemo(status.tx.memo);

  // filter affiliate txs from out_txs
  const userAddresses = new Set([
    status.tx.from_address.toLowerCase(),
    memo.destAddr?.toLowerCase(),
  ]);
  let outTxs = status.out_txs?.filter((tx) =>
    userAddresses.has(tx.to_address.toLowerCase()),
  );
  if (!outTxs) {
    outTxs = status.planned_out_txs
      ?.filter((tx) => userAddresses.has(tx.to_address.toLowerCase()))
      .map((tx) => ({
        ...tx,
        coins: [{ amount: tx.coin.amount, asset: tx.coin.asset }],
      }));
  }

  // extract max gas from in details actions
  const detailActions = inDetails?.actions?.filter((tx) =>
    userAddresses.has(tx.to_address.toLowerCase()),
  );
  let maxGas = 0;
  let maxGasAsset = null;
  if (detailActions?.length > 0) {
    maxGas = detailActions[0].max_gas[0].amount;
    maxGasAsset = parseAsset(detailActions[0].max_gas[0].asset, pools);
  }

  const inAsset = parseAsset(status.tx.coins[0].asset, pools);
  const inAmount = parseInt(status.tx.coins[0].amount || nativeIn.amount);
  const outAsset = parseAsset(
    outTxs?.length > 0 ? outTxs[0].coins[0].asset : memo.asset,
    pools,
  );
  const outAmount =
    outTxs?.length > 0
      ? parseInt(outTxs[0].coins[0].amount)
      : nativeOut?.amount;

  const outboundHasRefund = outTxs?.some(
    (tx) => tx.refund || tx.memo?.toLowerCase().startsWith("refund"),
  );
  const outboundHasSuccess = outTxs?.some((tx) =>
    tx.memo?.toLowerCase().startsWith("out"),
  );
  const outboundRefundReason = actions?.actions.find(
    (action) => action.type === "refund",
  )?.metadata.refund.reason;

  const state = {
    isSwap:
      memo.type.toLowerCase() === "swap" ||
      memo.type.toLowerCase() === "s" ||
      memo.type === "=",
    stage: null,
    memo: status.tx.memo,
    chain: status.tx.chain,
    inbound: {
      txid: status.tx.id,
      from: status.tx.from_address,
      asset: inAsset,
      amount: inAmount,
      usdValue: amountToUSD(inAmount, inAsset, pools),
      gas: status.tx.gas ? status.tx.gas[0].amount : nativeIn?.gas,
      gasAsset: status.tx.gas
        ? parseAsset(status.tx.gas[0].asset, pools)
        : nativeIn?.gasAsset,
      affiliate: memo.affiliate,
      preObservations: status.stages.inbound_observed?.pre_confirmation_count,
      observations: status.stages.inbound_observed?.final_count,
      confirmations: nativeIn?.confirmations,
      confirmationsRequired: nativeIn?.confirmationsRequired,
      finalisedHeight: inDetails?.finalised_height,
      icon: inAsset.symbol.toLowerCase(),
      done: status.stages.inbound_finalised?.completed,
    },
    swap: {
      limit: memo.limit,
      affiliateFee:
        parseInt(actions?.actions[0]?.metadata?.swap?.affiliateFee) || null,
      liquidityFee:
        parseInt(actions?.actions[0]?.metadata?.swap?.liquidityFee) || null,
      slip: parseInt(actions?.actions[0]?.metadata?.swap?.swapSlip),
      streaming: {
        count: status.stages.swap_status?.streaming?.count,
        interval:
          status.stages.swap_status?.streaming?.interval || memo.interval,
        quantity:
          status.stages.swap_status?.streaming?.quantity || memo.quantity,
      },
      done:
        status.stages.swap_finalised?.completed &&
        !status.stages.swap_status?.pending,
    },
    outbound: {
      txid:
        outTxs?.length > 0 && outTxs[0].chain !== "THOR" ? outTxs[0]?.id : null,
      to: (outTxs?.length > 0 && outTxs[0].to_address) || memo.destAddr,
      asset: outAsset,
      amount: parseInt(outAmount),
      usdValue: outAmount ? amountToUSD(outAmount, outAsset, pools) : null,
      maxGas: maxGas,
      maxGasAsset: maxGasAsset,
      gas:
        outTxs?.length > 0 && outTxs[0].gas
          ? outTxs[0].gas[0].amount
          : nativeOut?.gas,
      gasAsset:
        outTxs?.length > 0 && outTxs[0].gas
          ? parseAsset(outTxs[0].gas[0].asset, pools)
          : null,
      observations:
        (outDetails?.txs &&
          Math.max(...outDetails.txs.map((tx) => tx.signers.length))) ||
        0,
      confirmations: nativeOut?.confirmations,
      finalisedHeight: outDetails?.finalised_height,
      icon: outAsset.symbol?.toLowerCase(),
      delayBlocks: inDetails?.outbound_height - inDetails?.finalised_height,
      delayBlocksRemaining:
        status.stages.outbound_delay?.remaining_delay_blocks || 0,
      done:
        status.stages.swap_finalised?.completed &&
        !status.stages.swap_status?.pending &&
        (status.stages.outbound_signed?.completed ||
          outAsset.chain === "THOR" ||
          outAsset.synth),
      hasRefund: outboundHasRefund,
      hasSuccess: outboundHasSuccess,
      hasMultipleSuccess: outTxs?.length > 1,
      refundReason: outboundRefundReason,
    },
    extraOutbounds: outTxs?.slice(1).map((tx) => ({
      txid: tx.id,
      gas: tx.gas ? tx.gas[0].amount : null,
      gasAsset: tx.gas ? parseAsset(tx.gas[0].asset, pools) : null,
      to: tx.to_address,
      icon: parseAsset(tx.coins[0].asset, pools).symbol.toLowerCase(),
      asset: parseAsset(tx.coins[0].asset, pools),
      amount: parseInt(tx.coins[0].amount),
      usdValue: amountToUSD(
        parseInt(tx.coins[0].amount),
        parseAsset(tx.coins[0].asset, pools),
        pools,
      ),
    })),
  };

  return state;
}

////////////////////////////////////////////////////////////////////////////////////////
// Components
////////////////////////////////////////////////////////////////////////////////////////

const LongPressTooltip = ({
  label,
  placement = "top",
  justify = "right",
  children,
}) => {
  const [showLongPressTooltip, setShowLongPressTooltip] = React.useState(false);
  let pressTimer = null;

  const handleStart = () => {
    pressTimer = setTimeout(() => {
      setShowLongPressTooltip(true);
    }, 200); // Delay for long press (200ms)
  };

  const handleEnd = () => {
    clearTimeout(pressTimer);
    setShowLongPressTooltip(false);
  };

  React.useEffect(() => {
    return () => {
      clearTimeout(pressTimer); // Clear timeout if the component unmounts
    };
  }, [pressTimer]);

  return (
    <Tooltip label={label} isOpen={showLongPressTooltip} placement={placement}>
      <Flex
        onTouchStart={handleStart}
        onTouchEnd={handleEnd}
        onMouseEnter={handleStart}
        onMouseLeave={handleEnd}
        justifyContent={justify}
        // CSS to disable text selection
        sx={{
          WebkitUserSelect: "none" /* Safari */,
          KhtmlUserSelect: "none" /* Konqueror HTML */,
          MozUserSelect: "none" /* Old versions of Firefox */,
          MsUserSelect: "none" /* Internet Explorer/Edge */,
          userSelect: "none" /* Chrome, Edge, Opera and Firefox */,
        }}
      >
        {children}
      </Flex>
    </Tooltip>
  );
};

////////////////////////////////////////////////////////////////////////////////////////
// App
////////////////////////////////////////////////////////////////////////////////////////

function App() {
  // ------------------------------ params ------------------------------

  // get txid from first path param
  let txid = window.location.pathname.split("/")[1];

  // strip 0x prefix
  txid = stripTxID(txid);

  // ---------- query ----------

  // chain can be provided to show status before observation in thorchain
  const params = new URLSearchParams(window.location.search);
  const queryChain = params.get("chain");
  const queryNetwork = params.get("network");
  if (queryNetwork === "stagenet") {
    MidgardURL = "https://stagenet-midgard.ninerealms.com";
    ThornodeURL = "https://stagenet-thornode.ninerealms.com";
  }

  // allow whitelisted logos
  let queryLogo = params.get("logo");
  const logoWhitelist = [
    "9r.png",
    "asgardex.png",
    "shapeshift.png",
    "thorwallet.svg",
    "trust.svg",
  ];
  if (queryLogo && !logoWhitelist.includes(queryLogo)) {
    queryLogo = null;
  }

  // ------------------------------ ui state ------------------------------

  const [txidInput, setTxidInput] = React.useState("");
  const [accordionIndex, setAccordionIndex] = React.useState([]);
  const { activeStep, setActiveStep } = useSteps({
    index: 1,
    count: 3,
  });
  const [cloutSwitch, setCloutSwitch] = React.useState({
    Scheduled: false,
    Outbound: false,
  });

  // ------------------------------ static ------------------------------

  // ---------- network ----------

  const [network, setNetwork] = React.useState(null);
  const [inboundAddresses, setInboundAddresses] = React.useState(null);
  const [mimir, setMimir] = React.useState(null);

  React.useEffect(() => {
    const updateNetwork = async () => {
      setNetwork((await axios.get(`${MidgardURL}/v2/network`)).data);
    };
    updateNetwork();
    setInterval(updateNetwork, 300000); // 5 minutes
  }, [txid]);

  React.useEffect(() => {
    const updateInboundAddressesAndMimir = async () => {
      if (txid) return;

      // get mimirs and inbound addresses
      const mimir = (await axios.get(`${ThornodeURL}/thorchain/mimir`)).data;
      let inboundAddresses = (
        await axios.get(`${ThornodeURL}/thorchain/inbound_addresses`)
      ).data;

      // update mimir
      setMimir(mimir);

      // extend with halt heights
      inboundAddresses = inboundAddresses.map((chain) => ({
        ...chain,
        haltHeight: Math.max(
          ...Object.keys(mimir)
            .filter(
              (key) =>
                (new RegExp(`.*HALT.*${chain.chain}CHAIN`).test(key) ||
                  key === "HALTCHAINGLOBAL") &&
                mimir[key] !== 0,
            )
            .map((key) => mimir[key]),
        ),
        haltTradingHeight: Math.max(
          ...Object.keys(mimir)
            .filter(
              (key) =>
                (new RegExp(`HALT${chain.chain}TRADING`).test(key) ||
                  key === "HALTTRADING") &&
                mimir[key] !== 0,
            )
            .map((key) => mimir[key]),
        ),
        haltSigningHeight: Math.max(
          ...Object.keys(mimir)
            .filter(
              (key) =>
                new RegExp(`HALTSIGNING${chain.chain}`).test(key) &&
                mimir[key] !== 0,
            )
            .map((key) => mimir[key]),
        ),
        haltLPHeight: Math.max(
          ...Object.keys(mimir)
            .filter(
              (key) =>
                new RegExp(`PAUSELP${chain.chain}`).test(key) &&
                mimir[key] !== 0,
            )
            .map((key) => mimir[key]),
        ),
      }));

      setInboundAddresses(inboundAddresses);
    };
    updateInboundAddressesAndMimir();
    setInterval(updateInboundAddressesAndMimir, 600000); // 10 minutes
  }, [txid]);

  // ------------------------------ background intervals ------------------------------

  // intervals (clear once complete to stop polling)
  const poolsIntervalRef = React.useRef(null);

  // used or set in effects, no need to re-render
  const poolsRef = React.useRef(null);
  const actionsRef = React.useRef(null);
  const inboundDetailsRef = React.useRef(null);
  const outboundDetailsRef = React.useRef(null);
  const nativeInboundRef = React.useRef(null);
  const nativeOutboundRef = React.useRef(null);
  const heightRef = React.useRef(0);
  const startEtaRef = React.useRef(null);

  // state changes triggering render
  const [status, setStatus] = React.useState(null);
  const [pools, setPools] = React.useState(null);

  // derived
  const [state, setState] = React.useState(null);
  const [eta, setEta] = React.useState(null);
  const [age, setAge] = React.useState(null);

  // ------------------------------ pending ------------------------------

  // only fetched when txid is not present
  const [pending, setPending] = React.useState(null);
  React.useEffect(() => {
    if (txid) return;

    const updateOutbounds = async () => {
      const outboundReq = axios.get(`${ThornodeURL}/thorchain/queue/outbound`);
      const scheduledReq = axios.get(
        `${ThornodeURL}/thorchain/queue/scheduled`,
      );
      const streamingReq = axios.get(
        `${ThornodeURL}/thorchain/swaps/streaming`,
      );
      const [outboundRes, scheduledRes, streamingRes] = await Promise.all([
        outboundReq,
        scheduledReq,
        streamingReq,
      ]);

      for (const res of [outboundRes, scheduledRes, streamingRes]) {
        const newHeight = parseInt(res.headers["x-thorchain-height"], 10);
        heightRef.current = Math.max(heightRef.current, newHeight);
      }

      let pending = [];
      if (outboundRes?.data) {
        pending = pending.concat(
          outboundRes.data.map((pending) => ({
            ...pending,
            type: "Outbound",
          })),
        );
      }
      if (scheduledRes?.data) {
        pending = pending.concat(
          scheduledRes.data.map((pending) => ({
            ...pending,
            type: "Scheduled",
          })),
        );
      }
      if (streamingRes?.data) {
        pending = pending.concat(
          streamingRes.data.map((pending) => ({
            ...pending,
            type: "Streaming",
            sourceAsset: parseAsset(pending.source_asset, poolsRef.current),
            targetAsset: parseAsset(pending.target_asset, poolsRef.current),
          })),
        );
      }

      pending = pending.map((pending) =>
        pending.type === "Streaming"
          ? pending
          : {
              asset: parseAsset(pending.coin.asset, poolsRef.current),
              amount: pending.coin.amount,
              to: pending.to_address,
              memo: pending.memo,
              source: pending.memo.split(":")[1],
              type: pending.type,
              height: pending.height,
              vault: pending.vault_pub_key,
              cloutSpent: parseFloat(pending.clout_spent),
            },
      );

      setPending(pending);
    };

    updateOutbounds();
    setInterval(updateOutbounds, 15000);
  }, [txid]);

  // ------------------------------ background updates ------------------------------

  // ---------- pools (10m) ----------

  const updatePools = async () => {
    const data = (await axios.get(`${ThornodeURL}/thorchain/pools`)).data;
    const pools = {};
    data.forEach((pool) => {
      pools[pool.asset] = pool;
      pools[pool.asset].asset = parseAsset(pool.asset);
    });
    setPools(pools);
    poolsRef.current = pools;
  };
  React.useEffect(() => {
    updatePools();
    poolsIntervalRef.current = setInterval(updatePools, 600000); // 10 minutes
  }, []);

  // ---------- update (30s) ----------

  React.useEffect(() => {
    if (!txid) return;

    const update = async () => {
      const res = await axios.get(`${ThornodeURL}/thorchain/tx/status/${txid}`);
      const newHeight = parseInt(res.headers["x-thorchain-height"], 10);
      heightRef.current = Math.max(heightRef.current, newHeight);
      const partialState = makeState(
        res.data,
        poolsRef.current,
        null,
        null,
        null,
        null,
        null,
        null,
      );

      // update native inbound until finalized
      if (!partialState?.inbound.done || nativeInboundRef.current === null) {
        const inboundChain =
          (partialState?.inbound.asset.synth && "THOR") ||
          partialState?.inbound.asset.chain ||
          queryChain;
        if (txid && inboundChain) {
          nativeInboundRef.current = await nativeTx(txid, inboundChain);
        }
      }

      // update native outbound until finalized
      if (
        partialState?.outbound.txid &&
        partialState?.outbound.asset.chain !== "THOR" &&
        (!partialState?.outbound.done || nativeOutboundRef.current === null)
      ) {
        nativeOutboundRef.current = await nativeTx(
          partialState.outbound.txid,
          partialState.outbound.asset.chain,
        );
      }

      // update midgard actions for fees
      if (res.data.stages.swap_finalised?.completed) {
        const actions = await axios.get(`${MidgardURL}/v2/actions`, {
          params: { txid: txid },
        });
        actionsRef.current = actions.data;
      }

      // continue updating until done
      if (
        !res.data.stages.swap_finalised?.completed ||
        res.data.stages.swap_status?.pending ||
        res.data.stages.outbound_delay?.completed === false ||
        res.data.stages.outbound_signed?.completed === false
      ) {
        setTimeout(update, 30000); // 30 seconds
      } else {
        clearInterval(poolsIntervalRef.current);
      }

      // TODO: should go away after status provides finalized height
      const inDetails = (
        await axios.get(`${ThornodeURL}/thorchain/tx/details/${txid}`)
      ).data;
      inboundDetailsRef.current = inDetails;
      if (
        partialState?.outbound.txid &&
        partialState?.outbound.asset.chain !== "THOR"
      ) {
        const outDetails = (
          await axios.get(
            `${ThornodeURL}/thorchain/tx/details/${partialState?.outbound.txid}`,
          )
        ).data;
        outboundDetailsRef.current = outDetails;
      }

      // this triggers the state update,
      setStatus(res.data);
    };

    update();
  }, [txid, queryChain]);

  // ------------------------------ derived ------------------------------

  // ---------- state ----------

  React.useEffect(() => {
    const state = makeState(
      status,
      pools,
      nativeInboundRef.current,
      nativeOutboundRef.current,
      actionsRef.current,
      inboundDetailsRef.current,
      outboundDetailsRef.current,
    );
    setState(state);

    if (state?.isOutbound) {
      return;
    }

    // calculate eta
    const confirmTimeRemaining =
      Math.max(
        (state?.inbound.confirmationsRequired - state?.inbound.confirmations) *
          blockMilliseconds(state?.inbound.asset.chain),
        0,
      ) || 0;
    let streamTimeRemaining = 0;
    if (state?.swap.streaming) {
      const { quantity, count, interval } = state?.swap.streaming;
      const streamBlocksRemaining = (quantity - count) * interval;
      streamTimeRemaining =
        streamBlocksRemaining * blockMilliseconds("THOR") || 0;
    }
    const outboundDelayRemaining =
      (state?.outbound.delayBlocksRemaining || 0) * blockMilliseconds("THOR");
    setEta(confirmTimeRemaining + streamTimeRemaining + outboundDelayRemaining);

    // calculate age
    setAge(
      (heightRef.current - state?.inbound.finalisedHeight) *
        blockMilliseconds("THOR"),
    );

    // set the eta at the start for progress circle
    if (!startEtaRef.current) {
      startEtaRef.current =
        confirmTimeRemaining + streamTimeRemaining + outboundDelayRemaining;
    }
  }, [status, pools]);

  // ---------- eta ----------

  const ticker = () => {
    setEta((eta) => Math.max(0, eta - 1000));
    setAge((age) => age + 1000);
  };
  React.useEffect(() => {
    setInterval(ticker, 1000);
  }, []);

  // ------------------------------ steps ------------------------------

  React.useEffect(() => {
    if (state?.isOutbound) return;

    let stage = 0;

    if (state?.inbound.done) {
      stage = 1;
    }
    if (state?.swap.done) {
      stage = 2;
    }
    if (state?.outbound.done) {
      stage = 3 + state?.extraOutbounds?.length;
    }

    // skip if we have not progressed
    if (activeStep === stage) return;

    // close the previous accordion step
    let newIndex = [];
    if (Array.isArray(accordionIndex)) {
      newIndex = newIndex.filter((i) => i !== stage - 1).concat(stage);
    } else {
      newIndex = [stage];
    }

    setAccordionIndex(newIndex);
    setActiveStep(stage);
  }, [accordionIndex, activeStep, setActiveStep, state]);

  // ------------------------------ steps ------------------------------

  if (state?.isOutbound) {
    return (
      <ChakraProvider theme={theme}>
        <AlertDialog isOpen={true}>
          <AlertDialogOverlay>
            <AlertDialogContent width="100%">
              <AlertDialogHeader fontSize="md" fontWeight="bold">
                <HStack justify="space-between">
                  <Text>Detected Outbound Transaction</Text>
                  <Button
                    size="sm"
                    colorScheme="blue"
                    onClick={() => {
                      window.location.pathname = state.inbound;
                    }}
                  >
                    Go to Inbound
                  </Button>
                </HStack>
              </AlertDialogHeader>
            </AlertDialogContent>
          </AlertDialogOverlay>
        </AlertDialog>
      </ChakraProvider>
    );
  }

  const steps = [
    // ------------------------------ inbound ------------------------------
    {
      title: (
        <HStack justify="space-between" width="full">
          <Text>Inbound</Text>
          <LongPressTooltip label={txid} placement="top" maxW="none">
            <Link
              target="_blank"
              href={txExplorerLink(txid, state?.inbound.asset, queryNetwork)}
            >
              <Tag>
                <HStack>
                  <Text>... {txid.slice(-6)}</Text>
                  <ExternalLinkIcon />
                </HStack>
              </Tag>
            </Link>
          </LongPressTooltip>
        </HStack>
      ),
      rows: [
        {
          label: "Age",
          value: millisecondsToDHMS(age),
        },
        {
          label: "From",
          value: state?.inbound && (
            <LongPressTooltip
              label={state?.inbound.from}
              placement="top"
              maxW="none"
            >
              <Link
                target="_blank"
                href={addressExplorerLink(
                  state?.inbound.from,
                  state?.inbound.asset,
                  queryNetwork,
                )}
              >
                <Flex justify="right">
                  <Text>
                    {shortAddress(
                      state?.inbound.from,
                      state?.inbound.asset.chain,
                    )}
                  </Text>
                  <ExternalLinkIcon ml={2} />
                </Flex>
              </Link>
            </LongPressTooltip>
          ),
        },
        {
          label: "Gas",
          value: (
            <LongPressTooltip
              fontSize="md"
              placement="right"
              label={usdString(
                amountToUSD(state?.inbound.gas, state?.inbound.gasAsset, pools),
              )}
            >
              <Text>
                {state?.inbound.gas / 1e8} {state?.inbound.gasAsset?.symbol}
              </Text>
            </LongPressTooltip>
          ),
        },
        state?.swap.affiliateFee > 0 && {
          label: "Affiliate Fee",
          value: (
            <LongPressTooltip
              fontSize="md"
              placement="right"
              label={usdString(
                (state?.swap.affiliateFee / 10000) * state?.inbound.usdValue,
              )}
            >
              <Text>{(state?.swap.affiliateFee / 100).toFixed(2)}%</Text>
            </LongPressTooltip>
          ),
        },
        state?.inbound.affiliate && {
          label: "Affiliate",
          value: (
            <Text>
              {shortAddress(
                state?.inbound.affiliate,
                state?.inbound.asset.chain,
              )}
            </Text>
          ),
        },
        state?.inbound.asset.chain !== "THOR" &&
          !state?.inbound.asset.synth &&
          state?.inbound.preObservations && {
            label: "Pre-Confirm Observations",
            value:
              !state?.inbound.finalisedHeight ||
              heightRef.current - state?.inbound.finalisedHeight <
                ProgressBlocks ? (
                <HStack justify="right">
                  <Text size="sm">
                    {state?.inbound.preObservations}/{network?.activeNodeCount}{" "}
                    nodes
                  </Text>
                </HStack>
              ) : (
                <Text size="sm">{state?.inbound.preObservations} nodes</Text>
              ),
          },
        requiresConfirmations(state?.inbound.asset) && {
          label: "Confirmations",
          value:
            state?.inbound.confirmationsRequired >= 0 &&
            (!state?.inbound.finalisedHeight ||
              heightRef.current - state?.inbound.finalisedHeight <
                ProgressBlocks) ? (
              <HStack>
                <Progress
                  width="full"
                  hasStripe={
                    state?.inbound.confirmations <
                    state?.inbound.confirmationsRequired
                  }
                  colorScheme={
                    state?.inbound.confirmations >=
                    state?.inbound.confirmationsRequired
                      ? "green"
                      : "blue"
                  }
                  size="sm"
                  value={
                    (state?.inbound.confirmations /
                      state?.inbound.confirmationsRequired) *
                    100
                  }
                />
                <Text size="sm">
                  {state?.inbound.confirmations}/
                  {state?.inbound.confirmationsRequired} blocks
                </Text>
              </HStack>
            ) : state?.inbound.confirmations ? (
              <LongPressTooltip
                label={millisecondsToDHMS(
                  state?.inbound.confirmations *
                    blockMilliseconds(state?.inbound.asset.chain),
                )}
                placement="top"
                maxW="none"
              >
                <Flex justify="right">
                  <Text size="sm">{state?.inbound.confirmations} blocks</Text>
                </Flex>
              </LongPressTooltip>
            ) : (
              <Progress width="full" size="sm" isIndeterminate />
            ),
        },
        state?.inbound.asset.chain !== "THOR" &&
          !state?.inbound.asset.synth && {
            label: "Observations",
            value: (heightRef.current - state?.inbound.finalisedHeight <
              ProgressBlocks && (
              <HStack>
                <Progress
                  width="full"
                  hasStripe={
                    state?.inbound.observations < network?.activeNodeCount
                  }
                  colorScheme={
                    state?.inbound.observations >
                    (2 / 3) * network?.activeNodeCount
                      ? "green"
                      : "blue"
                  }
                  size="sm"
                  value={
                    (state?.inbound.observations / network?.activeNodeCount) *
                    100
                  }
                />
                <Text size="sm">
                  {state?.inbound.observations}/{network?.activeNodeCount} nodes
                </Text>
              </HStack>
            )) || <Text size="sm">{state?.inbound.observations} nodes</Text>,
          },
      ],
    },

    // ------------------------------ swap ------------------------------

    {
      title: "Swap",
      rows: [
        state?.swap.streaming.interval && {
          label: "Interval",
          value: (
            <Text size="sm">{state?.swap.streaming.interval} blocks/swap</Text>
          ),
        },
        state?.swap.streaming.quantity && {
          label: "Quantity",
          value: <Text size="sm">{state?.swap.streaming.quantity} swaps</Text>,
        },
        state?.swap.limit && {
          label: "Limit",
          value: (
            <Text size="sm">
              {(state?.swap.limit / 1e8).toLocaleString()}{" "}
              {state?.outbound.asset.symbol}
            </Text>
          ),
        },
        state?.inbound.done &&
          state?.swap.streaming.quantity && {
            label: "Stream",
            value: (
              <HStack>
                <Progress
                  width="full"
                  hasStripe={!state?.swap.done}
                  colorScheme={state?.swap.done ? "green" : "blue"}
                  size="sm"
                  value={
                    ((state?.swap.streaming.count ||
                      state?.swap.streaming.quantity) /
                      state?.swap.streaming.quantity) *
                    100
                  }
                />
                <Text size="sm">
                  {state?.swap.streaming.count ||
                    state?.swap.streaming.quantity}
                  /{state?.swap.streaming.quantity}
                </Text>
              </HStack>
            ),
          },
        state?.swap.liquidityFee && {
          label: "Liquidity Fee",
          value: (
            <LongPressTooltip
              fontSize="md"
              placement="right"
              label={usdString(
                amountToUSD(state?.swap.liquidityFee, RuneAsset, pools),
              )}
            >
              <Text>
                {`${
                  state?.swap.liquidityFee / 1e8 > 1000
                    ? "~" +
                      Math.round(
                        state?.swap.liquidityFee / 1e8,
                      ).toLocaleString()
                    : state?.swap.liquidityFee / 1e8
                } RUNE (${(state?.swap.slip / 100).toFixed(2)}%)`}
              </Text>
            </LongPressTooltip>
          ),
        },
      ],
    },
  ];

  // ------------------------------ outbounds ------------------------------

  const allOutbounds = state?.outbound
    ? [
        ...[state.outbound],
        ...(state.extraOutbounds ? state.extraOutbounds : []),
      ]
    : [];

  const consolidatedOutbounds = allOutbounds.reduce((acc, outbound) => {
    const key = assetChainSymbol(outbound.asset);
    if (acc[key]) {
      acc[key].amount += outbound.amount;
      acc[key].usdValue += outbound.usdValue;
    } else {
      acc[key] = { ...outbound }; // Clone the object to avoid side-effects
    }
    return acc;
  }, {});

  const summaryOutbounds = Object.values(consolidatedOutbounds);

  allOutbounds.map((outbound) => {
    steps.push({
      title: (
        <HStack justify="space-between" width="full">
          <Text>Outbound</Text>
          {activeStep >= 2 && outbound.txid && (
            <LongPressTooltip label={outbound.txid} placement="top" maxW="none">
              <Link
                target="_blank"
                href={txExplorerLink(
                  outbound.txid,
                  outbound.asset,
                  queryNetwork,
                )}
              >
                <Tag>
                  <HStack>
                    <Text>... {outbound.txid?.slice(-6)}</Text>
                    <ExternalLinkIcon />
                  </HStack>
                </Tag>
              </Link>
            </LongPressTooltip>
          )}
        </HStack>
      ),
      rows: [
        {
          label: "Destination",
          value: outbound && (
            <LongPressTooltip label={outbound.to} placement="top" maxW="none">
              <Link
                target="_blank"
                href={addressExplorerLink(
                  outbound.to,
                  outbound.asset,
                  queryNetwork,
                )}
              >
                {shortAddress(outbound.to, outbound.asset.chain)}
                <ExternalLinkIcon ml={2} />
              </Link>
            </LongPressTooltip>
          ),
        },
        (outbound.asset.chain !== "THOR" &&
          !outbound.asset.synth &&
          outbound.finalisedHeight &&
          outbound.maxGas && {
            label: "Max Gas",
            value: (
              <LongPressTooltip
                fontSize="md"
                placement="right"
                label={usdString(
                  amountToUSD(outbound.maxGas, outbound.gasAsset, pools),
                )}
              >
                <Text>
                  {outbound.maxGas / 1e8} {outbound.gasAsset.symbol}
                </Text>
              </LongPressTooltip>
            ),
          }) ||
          null,
        outbound.gas > 0 && {
          label: "Gas",
          value: (
            <LongPressTooltip
              fontSize="md"
              placement="right"
              label={usdString(
                amountToUSD(outbound.gas, outbound.gasAsset, pools),
              )}
            >
              <Text>
                {outbound.gas / 1e8} {outbound.gasAsset.symbol}
              </Text>
            </LongPressTooltip>
          ),
        },
        outbound.delayBlocks > 0 && {
          label: "Delay",
          value: ((outbound.delayBlocksRemaining > 0 ||
            heightRef.current - outbound.finalisedHeight < ProgressBlocks) && (
            <HStack>
              <Progress
                width="full"
                hasStripe={outbound.delayBlocksRemaining > 0}
                colorScheme={
                  outbound.delayBlocksRemaining === 0 ? "green" : "blue"
                }
                size="sm"
                value={
                  ((outbound.delayBlocks - outbound.delayBlocksRemaining) /
                    outbound.delayBlocks) *
                  100
                }
              />
              <LongPressTooltip
                label={millisecondsToDHMS(
                  outbound.delayBlocksRemaining * blockMilliseconds("THOR"),
                )}
                placement="top"
              >
                <Text size="sm">
                  {outbound.delayBlocks - outbound.delayBlocksRemaining}/
                  {outbound.delayBlocks} blocks
                </Text>
              </LongPressTooltip>
            </HStack>
          )) || (
            <HStack justify="right">
              <Text size="sm">{outbound.delayBlocks} blocks</Text>
            </HStack>
          ),
        },
        outbound.asset.chain !== "THOR" &&
          !outbound.asset.synth &&
          outbound.delayBlocksRemaining === 0 && {
            label: "Observations",
            value: (heightRef.current - outbound.finalisedHeight <
              ProgressBlocks && (
              <HStack>
                <Progress
                  width="full"
                  hasStripe={outbound.observations < network?.activeNodeCount}
                  isIndeterminate={outbound.observations === 0}
                  colorScheme={
                    outbound.observations > (2 / 3) * network?.activeNodeCount
                      ? "green"
                      : "blue"
                  }
                  size="sm"
                  value={
                    (outbound.observations / network?.activeNodeCount) * 100
                  }
                />
                <Text size="sm">
                  {outbound.observations}/{network?.activeNodeCount} nodes
                </Text>
              </HStack>
            )) || <Text size="sm">{outbound.observations} nodes</Text>,
          },
        outbound.delayBlocksRemaining === 0 &&
          requiresConfirmations(outbound.asset) && {
            label: "Confirmations",
            value: (outbound.confirmations && (
              <LongPressTooltip
                label={millisecondsToDHMS(
                  outbound.confirmations *
                    blockMilliseconds(outbound.asset.chain),
                )}
                placement="top"
                maxW="none"
              >
                <Flex justify="right">
                  <Text size="sm">{outbound.confirmations} blocks</Text>
                </Flex>
              </LongPressTooltip>
            )) || <Progress width="full" size="sm" isIndeterminate />,
          },
      ],
    });

    return null;
  });

  // ------------------------------ progress ------------------------------

  let progress = null;
  if (eta > 0) {
    progress = (
      <HStack>
        <CircularProgress
          value={100 - (100 * eta) / startEtaRef.current}
          size="32px"
        />
        <Heading size="md">{eta ? millisecondsToDHMS(eta) : "..."}</Heading>
      </HStack>
    );
  } else if (activeStep < 3) {
    progress = (
      <Button
        size="sm"
        isLoading
        colorScheme={state?.outbound.hasRefund ? "yellow" : "blue"}
        width="33%"
        spinner={<BeatLoader size={8} color="white" />}
      ></Button>
    );
  } else if (
    (state?.outbound.hasRefund && state?.outbound.hasSuccess) ||
    state?.outbound.hasMultipleSuccess
  ) {
    progress = (
      <Tag colorScheme="orange" size="lg" variant="subtle">
        <Text>Partial Fill</Text>
      </Tag>
    );
  } else if (state?.outbound.hasRefund) {
    progress = (
      <LongPressTooltip label={state?.outbound.refundReason} placement="bottom">
        <Tag colorScheme="red" size="lg">
          <Text>Swap Refunded</Text>
        </Tag>
      </LongPressTooltip>
    );
  } else if (state?.outbound.hasSuccess) {
    progress = (
      <Tag colorScheme="green" size="lg" variant="subtle">
        <Text>Success</Text>
      </Tag>
    );
  }

  // ------------------------------ card content ------------------------------

  let content = null;
  if (!isValidTxID(txid)) {
    content = (
      <Box width="100%" maxWidth="3xl">
        <Card variant="outline" my={3} p={3}>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              if (isValidTxID(stripTxID(txidInput))) {
                window.location.pathname = stripTxID(txidInput);
              }
            }}
          >
            <FormControl
              isInvalid={
                txidInput.length > 0 && !isValidTxID(stripTxID(txidInput))
              }
            >
              <HStack>
                <Input
                  type="text"
                  placeholder="Swap Transaction ID"
                  value={txidInput}
                  onChange={(e) => setTxidInput(e.target.value)}
                />
                <Button
                  type="submit"
                  isDisabled={!isValidTxID(stripTxID(txidInput))}
                  colorScheme="blue"
                >
                  Track
                </Button>
              </HStack>
            </FormControl>
          </form>
        </Card>

        <Card variant="outline" my={3} p={3}>
          <Flex>
            <Heading size="sm">Chain Status</Heading>
            <Spacer />
            <Center>
              <SkeletonCircle size={3} mr={1} />
            </Center>
          </Flex>
          <Divider my={3} />

          <TableContainer>
            <Table size="sm">
              <Thead>
                <Tr>
                  <Th p={1}></Th>
                  {inboundAddresses?.map((chain) => (
                    <Th p={1} textAlign="center">
                      {chain.chain}
                    </Th>
                  ))}
                </Tr>
              </Thead>
              <Tbody>
                <Tr>
                  <Th p={1}>Scanning</Th>
                  {inboundAddresses?.map((chain) => (
                    <Td p={2} textAlign="center">
                      {chain.haltHeight > heightRef.current ? (
                        <LongPressTooltip
                          justify="center"
                          placement="top"
                          label={haltTooltip(
                            chain.haltHeight,
                            heightRef.current,
                          )}
                        >
                          <InfoIcon color="orange.400" />
                        </LongPressTooltip>
                      ) : chain.haltHeight > 0 ? (
                        <WarningIcon color="red.400" />
                      ) : (
                        <CheckCircleIcon color="green.400" />
                      )}
                    </Td>
                  ))}
                </Tr>
                <Tr>
                  <Th p={1}>Trading</Th>
                  {inboundAddresses?.map((chain) => (
                    <Td p={2} textAlign="center">
                      {chain.haltTradingHeight > heightRef.current ? (
                        <LongPressTooltip
                          justify="center"
                          placement="top"
                          label={haltTooltip(
                            chain.haltTradingHeight,
                            heightRef.current,
                          )}
                        >
                          <InfoIcon color="orange.400" />
                        </LongPressTooltip>
                      ) : chain.haltTradingHeight > 0 ? (
                        <Text>
                          <WarningIcon color="red.400" />
                        </Text>
                      ) : (
                        <CheckCircleIcon color="green.400" />
                      )}
                    </Td>
                  ))}
                </Tr>
                <Tr>
                  <Th p={1}>Deposit / Withdraw</Th>
                  {inboundAddresses?.map((chain) => (
                    <Td p={2} textAlign="center">
                      {chain.haltLPHeight > heightRef.current ? (
                        <LongPressTooltip
                          justify="center"
                          placement="top"
                          label={haltTooltip(
                            chain.haltLPHeight,
                            heightRef.current,
                          )}
                        >
                          <InfoIcon color="orange.400" />
                        </LongPressTooltip>
                      ) : chain.haltLPHeight > 0 ? (
                        <WarningIcon color="red.400" />
                      ) : (
                        <CheckCircleIcon color="green.400" />
                      )}
                    </Td>
                  ))}
                </Tr>
                <Tr>
                  <Th p={1}>Signing</Th>
                  {inboundAddresses?.map((chain) => (
                    <Td p={2} textAlign="center">
                      {chain.haltSigningHeight > heightRef.current ? (
                        <LongPressTooltip
                          justify="center"
                          placement="top"
                          label={haltTooltip(
                            chain.haltSigningHeight,
                            heightRef.current,
                          )}
                        >
                          <InfoIcon color="orange.400" />
                        </LongPressTooltip>
                      ) : chain.haltSigningHeight > 0 ? (
                        <Text>
                          <WarningIcon color="red.400" />
                        </Text>
                      ) : (
                        <CheckCircleIcon color="green.400" />
                      )}
                    </Td>
                  ))}
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>
        </Card>

        <Card variant="outline" my={3} p={3}>
          <Flex>
            <Heading size="sm">Network</Heading>
            <Spacer />
            <Center>
              <SkeletonCircle size={3} mr={1} />
            </Center>
          </Flex>
          <Divider my={3} />
          <HStack alignItems="stretch" overflowX="auto">
            <StatGroup
              borderWidth="1px"
              borderRadius="lg"
              p={3}
              backgroundColor="gray.50"
              flex="1"
              minWidth="160px"
            >
              <Stat>
                <StatLabel>Validators</StatLabel>
                <StatNumber>{network?.activeNodeCount} active</StatNumber>
                <StatHelpText>{network?.standbyNodeCount} standby</StatHelpText>
              </Stat>
            </StatGroup>
            <StatGroup
              borderWidth="1px"
              borderRadius="lg"
              p={3}
              backgroundColor="gray.50"
              flex="1"
              minWidth="160px"
            >
              <Stat>
                <StatLabel>Churn</StatLabel>
                <StatNumber>
                  <Flex alignItems="center">
                    {millisecondsToDHMS(
                      (parseInt(network?.nextChurnHeight) - heightRef.current) *
                        blockMilliseconds("THOR"),
                      true,
                    )}
                    {mimir?.HALTCHURNING && (
                      <LongPressTooltip
                        placement="right"
                        label="churning is halted"
                      >
                        <WarningIcon maxHeight="18px" ml={2} color="red.400" />
                      </LongPressTooltip>
                    )}
                  </Flex>
                </StatNumber>
                <StatHelpText>
                  {parseInt(network?.nextChurnHeight) - heightRef.current}{" "}
                  blocks
                </StatHelpText>
              </Stat>
            </StatGroup>
            <StatGroup
              borderWidth="1px"
              borderRadius="lg"
              p={3}
              backgroundColor="gray.50"
              flex="1"
              minWidth="160px"
            >
              <Stat>
                <StatLabel>Bond APY</StatLabel>
                <StatNumber>
                  {(parseFloat(network?.bondingAPY) * 100).toFixed(2)}%
                </StatNumber>
                <StatHelpText>
                  {((1 - parseFloat(network?.poolShareFactor)) * 100).toFixed(
                    2,
                  )}
                  % of emissions
                </StatHelpText>
              </Stat>
            </StatGroup>
            <StatGroup
              borderWidth="1px"
              borderRadius="lg"
              p={3}
              backgroundColor="gray.50"
              flex="1"
              minWidth="160px"
            >
              <Stat>
                <StatLabel>Liquidity APY</StatLabel>
                <StatNumber>
                  {`${(parseFloat(network?.liquidityAPY) * 100).toFixed(2)}%`}
                </StatNumber>
                <StatHelpText>
                  {(parseFloat(network?.poolShareFactor) * 100).toFixed(2)}% of
                  emissions
                </StatHelpText>
              </Stat>
            </StatGroup>
          </HStack>
        </Card>

        <Card variant="outline" my={3} p={3}>
          <Flex>
            <Heading size="sm">Streaming Swaps</Heading>
            <Spacer />
            <Center>
              <SkeletonCircle size={3} mr={1} />
            </Center>
          </Flex>
          <Divider my={3} />
          <TableContainer>
            <TxTable
              columns={[
                {
                  Header: "Age",
                  accessor: "age",
                  Cell: ({ value }) => millisecondsToDHMS(value, true),
                },
                {
                  Header: "ETA",
                  accessor: "eta",
                  Cell: ({ value }) => millisecondsToDHMS(value, true),
                },
                {
                  Header: "Source",
                  accessor: "sourceAsset",
                },
                {
                  Header: "Target",
                  accessor: "targetAsset",
                },
                {
                  Header: "USD Value",
                  accessor: "usdValue",
                  Cell: ({ value }) => usdString(value),
                },
                // Avoid some noise for now.
                // { Header: "Progress", accessor: "progress" },
                { Header: "Interval", accessor: "interval" },
                { Header: "Destination", accessor: "destination" },
                {
                  Header: "TxID",
                  accessor: "txid",
                  Cell: ({ value }) => "..." + value.slice(-6),
                },
                {
                  Header: "",
                  accessor: "extra",
                  Cell: ({ row }) => (
                    <Flex justifyContent="flex-end">
                      <ExternalLinkIcon />
                    </Flex>
                  ),
                },
              ]}
              data={
                pending
                  ?.filter((x) => x.type === "Streaming")
                  .map((row, index) => ({
                    id: index,
                    age: row.count * row.interval * blockMilliseconds("THOR"),
                    eta:
                      (row.quantity - row.count) *
                      row.interval *
                      blockMilliseconds("THOR"),
                    usdValue: amountToUSD(
                      parseInt(row.deposit),
                      row.sourceAsset,
                      pools,
                    ),
                    progress: `${row.count}/${row.quantity}`,
                    interval: row.interval,
                    txid: row.tx_id,
                    sourceAsset: `${assetChainSymbol(row.sourceAsset)}`,
                    targetAsset: `${assetChainSymbol(row.targetAsset)}`,
                    destination: shortAddress(
                      row.destination,
                      row.targetAsset.chain,
                    ),
                  })) || []
              }
            />
          </TableContainer>
        </Card>
        {["Outbound", "Scheduled"].map((type) => (
          <Card variant="outline" width="100%" my={3} p={3}>
            <Flex overflowX="auto">
              <Center>
                <Heading size="sm" mr={2} style={{ whiteSpace: "nowrap" }}>
                  <Text>{type} Queue</Text>
                </Heading>
              </Center>
              <Spacer />
              <HStack>
                {pending?.filter((x) => x.type === type).length > 0 && (
                  <>
                    <Tag style={{ whiteSpace: "nowrap" }}>
                      {`Value: ${usdString(
                        pending
                          ?.filter((x) => x.type === type)
                          .map((row, index) =>
                            amountToUSD(row.amount, row.asset, pools),
                          )
                          .reduce((acc, amount) => {
                            if (amount) {
                              return acc + amount;
                            }
                            return acc;
                          }, 0),
                      )}`}
                    </Tag>

                    <Popover>
                      <PopoverTrigger>
                        <Tag style={{ whiteSpace: "nowrap" }} as="button">
                          {`Outbounds: ${
                            pending?.filter((x) => x.type === type).length
                          }`}
                          <InfoIcon ml={2} size="sm" color="gray.400" />
                        </Tag>
                      </PopoverTrigger>
                      <PopoverContent width="100%">
                        <PopoverArrow />
                        <PopoverBody>
                          <VStack>
                            {pending
                              ? Object.entries(
                                  pending
                                    .filter((x) => x.type === type)
                                    .reduce((acc, row) => {
                                      if (
                                        acc[row.vault.slice(-4)] === undefined
                                      ) {
                                        acc[row.vault.slice(-4)] = 0;
                                      }
                                      acc[row.vault.slice(-4)]++;
                                      return acc;
                                    }, {}),
                                )
                                  .sort((a, b) => b[1] - a[1])
                                  .map(([vault, count]) => (
                                    <Tag
                                      fontFamily="mono"
                                      backgroundColor={colorizeVault(
                                        vault.slice(-4),
                                      )}
                                    >
                                      {vault}:{count}
                                    </Tag>
                                  ))
                              : null}
                          </VStack>
                        </PopoverBody>
                      </PopoverContent>
                    </Popover>

                    <Tag style={{ whiteSpace: "nowrap" }}>
                      {`Clout: ${(
                        pending
                          ?.filter((x) => x.type === type)
                          .reduce((acc, row) => {
                            if (row?.cloutSpent) {
                              return acc + row.cloutSpent;
                            }
                            return acc;
                          }, 0) / 1e8
                      ).toLocaleString("en-US", { maximumFractionDigits: 0 })}`}
                      <Switch
                        ml={2}
                        size="sm"
                        isChecked={cloutSwitch[type]}
                        onChange={() =>
                          setCloutSwitch((state) => ({
                            ...state,
                            [type]: !state[type],
                          }))
                        }
                      />
                    </Tag>
                  </>
                )}
                <Spacer />
                <Center>
                  <SkeletonCircle size={3} mr={1} />
                </Center>
              </HStack>
            </Flex>
            <Divider my={3} />
            <TableContainer>
              <TxTable
                columns={[
                  {
                    Header: type === "Scheduled" ? "ETA" : "Age",
                    accessor: "etaOrAge",
                    Cell: ({ value }) => millisecondsToDHMS(value, true),
                  },
                  { Header: "Type", accessor: "type" },
                  { Header: "Asset", accessor: "asset" },
                  {
                    Header: "USD Value",
                    accessor: "usdValue",
                    Cell: ({ value }) => usdString(value),
                  },
                  {
                    Header: "Clout Spent",
                    accessor: "cloutSpent",
                    show: cloutSwitch[type],
                    Cell: ({ value }) =>
                      value / 1e8 > 0
                        ? (value / 1e8).toLocaleString("en-US", {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })
                        : null,
                  },
                  { Header: "Destination", accessor: "destination" },
                  {
                    Header: "TxID",
                    accessor: "txid",
                    Cell: ({ value }) => "..." + value.slice(-6),
                  },
                  {
                    Header: "",
                    accessor: "extra",
                    Cell: ({ row }) => (
                      <HStack justifyContent="flex-end">
                        <LongPressTooltip
                          placement="top"
                          label={row.original.vault.slice(-4)}
                        >
                          <LockIcon
                            m={0}
                            mr={1}
                            p={0}
                            color={colorizeVault(row.original.vault.slice(-4))}
                          />
                        </LongPressTooltip>
                        <ExternalLinkIcon />
                      </HStack>
                    ),
                  },
                ]}
                data={
                  pending
                    ?.filter(
                      (x) =>
                        x.type === type &&
                        (type === "Outbound" || x.height > heightRef.current),
                    )
                    .map((row, index) => ({
                      id: index,
                      etaOrAge:
                        Math.abs(row.height - heightRef.current) *
                        blockMilliseconds("THOR"),
                      type: row.memo.split(":")[0],
                      asset: `${assetChainSymbol(row.asset)}`,
                      usdValue: amountToUSD(row.amount, row.asset, pools),
                      cloutSpent: row.cloutSpent,
                      destination: shortAddress(row.to, row.asset.chain),
                      txid: row.source,
                      vault: row.vault,
                    })) || []
                }
              />
            </TableContainer>
          </Card>
        ))}
      </Box>
    );
  } else if (!state) {
    content = (
      <Card variant="outline" height="md" width="sm" p={3}>
        <Center height="full">
          <Spinner size="xl" />
        </Center>
      </Card>
    );
  } else if (!state.isSwap) {
    content = (
      <Card variant="outline" width="sm" p={3}>
        <CardHeader>
          <Alert
            status="error"
            variant="subtle"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            textAlign="center"
          >
            <AlertIcon boxSize="40px" mr={0} />
            <AlertTitle mt={4} mb={1} fontSize="lg">
              Unsupported Transaction
            </AlertTitle>
            <AlertDescription maxWidth="sm">
              Tracker only supports swaps.
            </AlertDescription>
          </Alert>
        </CardHeader>

        <CardBody>
          <Stack divider={<StackDivider />} spacing="4">
            <Box>
              <Heading size="xs" textTransform="uppercase">
                TX ID
              </Heading>
              <Text pt="2" fontSize="sm">
                {state.inbound.txid}
              </Text>
            </Box>
            <Box>
              <Heading size="xs" textTransform="uppercase">
                Chain
              </Heading>
              <Text pt="2" fontSize="sm">
                {state.chain}
              </Text>
            </Box>
            <Box>
              <Heading size="xs" textTransform="uppercase">
                From Address
              </Heading>
              <Text pt="2" fontSize="sm">
                {state.inbound.from}
              </Text>
            </Box>
            <Box>
              <Heading size="xs" textTransform="uppercase">
                Memo
              </Heading>
              <Text pt="2" fontSize="sm">
                {state.memo}
              </Text>
            </Box>
            <Box>
              <Link href={runescanURL(`tx/${txid}`, queryNetwork)} isExternal>
                <Button fontWeight="normal" size="sm" colorScheme="gray">
                  <HStack>
                    <Text>View on Runescan</Text>
                    <ExternalLinkIcon />
                  </HStack>
                </Button>
              </Link>
            </Box>
          </Stack>
        </CardBody>
      </Card>
    );
  } else {
    content = (
      <Box>
        {queryLogo && (
          <Flex justifyContent="center" m={6}>
            <Image
              src={`/logos/${queryLogo}`}
              maxHeight="50px"
              maxWidth="150px"
            />
          </Flex>
        )}
        <Card variant="outline" width="sm" p={3}>
          <HStack justify="space-between" width="full">
            {progress}
            <Spacer />
            <Link href={runescanURL(`tx/${txid}`, queryNetwork)} isExternal>
              <Button fontWeight="normal" size="sm" colorScheme="gray">
                <HStack>
                  <Text>Runescan</Text>
                  <ExternalLinkIcon />
                </HStack>
              </Button>
            </Link>
          </HStack>
          <Divider mt={3} mb={3} />
          <HStack pt={2} px={1} pb={0}>
            <VStack>
              <Stat>
                <StatLabel>
                  <Flex align="center">
                    <Icon name={state?.inbound.icon} size={18} />
                    <Text ml={2}>{assetChainSymbol(state?.inbound.asset)}</Text>
                  </Flex>
                </StatLabel>
                <StatNumber ph={2}>
                  <Text textOverflow={"ellipsis"} isTruncated>
                    {state?.inbound.amount / 1e8}
                  </Text>
                </StatNumber>
                <StatHelpText>
                  {usdString(state?.inbound.usdValue)}
                </StatHelpText>
              </Stat>
            </VStack>
            <Box flexGrow={1}>
              <Center>
                <ArrowRightIcon />
              </Center>
            </Box>
            <VStack alignItems="flex-start" spacing="0">
              {summaryOutbounds.map((outbound, index) => (
                <Box>
                  {index > 0 && <Divider mt={1} mb={3} />}
                  <Stat>
                    <StatLabel>
                      <Flex align="center">
                        <Icon name={outbound.icon} size={18} />
                        <Text ml={2}>{assetChainSymbol(outbound.asset)}</Text>
                      </Flex>
                    </StatLabel>
                    <StatNumber>
                      <Text textOverflow={"ellipsis"} isTruncated>
                        {activeStep > 1 && outbound.amount
                          ? outbound.amount / 1e8
                          : "..."}
                      </Text>
                    </StatNumber>
                    <StatHelpText>
                      {activeStep > 1 && outbound.usdValue
                        ? usdString(outbound.usdValue)
                        : "..."}
                    </StatHelpText>
                  </Stat>
                </Box>
              ))}
            </VStack>
          </HStack>
          <Divider mt={2} mb={3} />
          <Stepper size="sm" index={activeStep} gap="0">
            {steps.slice(0, 3).map((step, index) => (
              <Step key={index} gap="0">
                <StepIndicator bg="white">
                  <StepStatus complete={<StepIcon />} />
                </StepIndicator>
                <StepSeparator _horizontal={{ ml: "0" }} />
              </Step>
            ))}
          </Stepper>
          <Accordion
            mt={3}
            index={accordionIndex}
            allowToggle
            onChange={setAccordionIndex}
          >
            {steps.map((step, index) => (
              <AccordionItem key={index}>
                <h2>
                  <AccordionButton>
                    <Box
                      as="span"
                      flex="1"
                      color={
                        activeStep === index && activeStep <= 2
                          ? ""
                          : "gray.400"
                      }
                      textAlign="left"
                    >
                      {step.title}
                    </Box>
                    {activeStep > 2 || activeStep > index ? (
                      <CheckIcon color="green.400" ml={3} mr={3} />
                    ) : activeStep === index ? (
                      <Spinner size="sm" ml={3} mr={3} />
                    ) : null}
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel p={2} bg="gray.50">
                  <TableContainer>
                    <Table size="sm">
                      <Tbody>
                        {step.rows.map(
                          (row, index) =>
                            row && (
                              <Tr>
                                <HStack
                                  fontFamily="mono"
                                  width="full"
                                  fontSize="sm"
                                  letterSpacing="normal"
                                >
                                  <Th p={2}>{row.label}</Th>
                                  <Box width="full" textAlign="right">
                                    {row.value}
                                  </Box>
                                </HStack>
                              </Tr>
                            ),
                        )}
                      </Tbody>
                    </Table>
                  </TableContainer>
                </AccordionPanel>
              </AccordionItem>
            ))}
          </Accordion>
        </Card>
      </Box>
    );
  }

  // ------------------------------ wrapper ------------------------------

  return (
    <ChakraProvider theme={theme}>
      <Box
        display="flex"
        flexDirection="column"
        minHeight="100vh"
        justifyContent="center"
        alignItems="center"
        bg="gray.100"
        fontFamily="Roboto"
        p={2}
      >
        {content}
      </Box>
    </ChakraProvider>
  );
}

export default App;
